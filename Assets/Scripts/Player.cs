﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player: MonoBehaviour {

    public float velocity;
    public float limitx;
    private float axis;

	// Update is called once per frame
	void Update () {

        axis = Input.GetAxis("Horizontal");

        transform.Translate(velocity * axis * Time.deltaTime, 0, 0);

        if(transform.position.x > limitx)
        {
            transform.position = new Vector3(limitx, transform.position.y, transform.position.z);
        }
        else if(transform.position.x < -limitx)
        {
            transform.position = new Vector3(-limitx, transform.position.y, transform.position.z);
        }


	}
}
